﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using KAD_kockanap.Connection;

namespace KAD_kockanap
{
    class Listener
    {

        UdpClient incoming;
        IPEndPoint ep;
        private int selfID;

        public int SelfID
        {
            get { return selfID; }
            set { selfID = value; }
        }
        
        private void Receiving(IAsyncResult ar)
        {
            UdpClient u = (UdpClient)((UdpState)(ar.AsyncState)).u;
            IPEndPoint e = (IPEndPoint)((UdpState)(ar.AsyncState)).e;
            byte[] received = u.Receive( ref e);
            string[] s = new string[16];
            int i = 0;
            foreach (byte bajt in received)
            {

                s[i] = bajt.ToString();
                i++;
            }
        }
        public void Listening()
        {
            
            UdpState s=new UdpState();
            s.u=incoming;
            s.e=ep;
            incoming.BeginReceive(new AsyncCallback(Receiving),s);
        }
        public int Connect()
        {
            incoming.Send(new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0x01, 0x16, 0x2A, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, 16);
            return incoming.Receive(ref ep)[5];
        }
        public Listener()
        {
             ep = new IPEndPoint(IPAddress.Parse("10.4.11.114"), 5674);
             incoming = new UdpClient(ep);
             selfID = Connect();
        }
        
        


        
        

    }
}

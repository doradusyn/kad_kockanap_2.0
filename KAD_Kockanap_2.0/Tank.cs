﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace KAD_Kockanap_2._0
{
    class Tank
    {
        float xCenter;
        float yCenter;
        int bulletNo;
        int rocketNo;
        int speed;
        int ownerID;
        char itemChar;
        float negativeX;
        float positiveX;
        float negativeY;
        float positiveY;
        byte[] utasitas;
        UdpClient kuldo;
        
        public Tank(byte portUpper,byte portLower,UdpClient kuldoclient)
        {
           
            this.utasitas = new byte[16];
            this.utasitas[0] = 0xFF;
            this.utasitas[1] = 0xFF;
            this.utasitas[2] = 0xFF;
            this.utasitas[3] = 0xFF;
            this.utasitas[4] = 0x02;
            this.utasitas[5] = portUpper;
            this.utasitas[6] = portLower;
            this.utasitas[9] = 0;
            this.utasitas[10] = 0;
            this.utasitas[11] = 0;
            this.utasitas[12] = 0;
            this.utasitas[13] = 0;
            this.utasitas[14] = 0;
            this.utasitas[15] = 0;
            this.kuldo=kuldoclient;
            
        }
        public Byte[] Rotate(int szog)
        {
            string hex = (szog).ToString("X");
            this.utasitas[7] = 0x02;
            this.utasitas[8] = byte.Parse(hex);
            return this.utasitas;
        }
        public Byte[] SetMovementSpeed(int szazalek)
        {
            this.utasitas[7] = 1;
            string hexpercent=(szazalek+100).ToString("X");
            this.utasitas[8] = byte.Parse(hexpercent);
            return this.utasitas;
        }
        public Byte[] ShootBullet()
        {
            this.utasitas[4] = 0x02;
            this.utasitas[7] = 0x03;
            return this.utasitas;
        }

        public Byte[] ShootRocket()
        {
            this.utasitas[4] = 0x02;
            this.utasitas[7] = 0x04;
            return this.utasitas;
        }

        public void Derekszogkanyar(int irany)
        {
            int szog = 0;
            switch (irany)
            {
                case 1:  //jobb
                    szog = 9;
                    break;
                case 2:
                    szog = 27;
                    break;
                default:
                    break;
            }
            kuldo.Send(this.SetMovementSpeed(100),16);
            //while (nemvagyokott)
            //{
                    
            //}
            kuldo.Send(this.SetMovementSpeed(0), 16);
            kuldo.Send(this.Rotate(szog), 16);
            kuldo.Send(this.SetMovementSpeed(100), 16);
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace KAD_Kockanap_2._0
{
    class Communication
    {
        public void Szar()
        {
            UdpClient kliens = new UdpClient();
            IPEndPoint iep = new IPEndPoint(IPAddress.Parse("10.4.11.24"), 11111);
            kliens.Connect(iep);
            IPEndPoint ep = new IPEndPoint(IPAddress.Parse("10.4.11.46"), 5674);
            kliens.Send(new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0x01, 0x16, 0x2A, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, 16);
            //kliens.Send(new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0x03, 0x1F, 0x90, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, 16);
            kliens.Send(new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0x02, 0x16, 0x2A, 0x03, 0, 0, 0, 0, 0, 0, 0, 0 }, 16);
            kliens.Close();
            kliens = new UdpClient(ep);
            StreamWriter sw = new StreamWriter("inc.txt", true);
            while (true)
            {
                byte[] received = kliens.Receive(ref ep);

                foreach (byte item in received)
                {
                    sw.Write(item + " ");
                }
                sw.WriteLine();
            }
        }
    }
}
